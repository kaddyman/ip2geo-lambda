FROM amazonlinux:latest

RUN yum -y install python3 \
    python3-pip \
    zip \
    gcc \
    python3-devel \
    && yum clean all

ADD app /app

RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install -r /app/requirements.txt --target /app
RUN pip list

RUN cd /app && zip -r ../ip2geo-lambda.zip *
