import geoip2.webservice, argparse, re, ujson, logging, os, boto3, time
from minfraud import Client
from botocore.exceptions import ClientError
from botocore.client import Config
from boto3.dynamodb.conditions import Key, Attr

try:
  root = logging.getLogger()
  if root.handlers:
    for handler in root.handlers:
      root.removeHandler(handler)
  logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',level=logging.INFO)
except Exception as e:
  print(f"FAILED to set basic logging config")
  print(f"{e}")
  exit(1)
else:
  logging.info(f"SUCCESSFUL set logging config")

try:
  if ('MAXUSERID' in os.environ) and ('MAXKEY' in os.environ) and ('DDB_TABLE' in os.environ) and ('DDB_KEY' in os.environ):
    maxuserid = os.environ['MAXUSERID']
    maxkey = os.environ['MAXKEY']
    ddb_table  = os.environ['DDB_TABLE']
    primary_key = os.environ['DDB_KEY']
    aws_region = os.environ['REGION']
  else:
    logging.critical("missing required env args")
    exit(1)
except Exception as e:
  logging.critical(f"FAILED to lookup ENV vars {e}")
  exit(1)

try:
  dynamodb = boto3.resource('dynamodb', region_name=aws_region)
  db_resource = dynamodb.Table(ddb_table)
  aws_session = boto3.session.Session()
except Exception as e:
  logging.critical(f"FAILED to setup DynamoDB resource. DDB_TABLE={ddb_table}, DDB_KEY={primary_key}, AWS_REGION={aws_region}. Message: {e}")
  exit(1)
else:
  logging.info(f"SUCCESSFUL created DynamoDB resource")

def _ipscore(**kwargs):
    self_ip = kwargs['myip']
    self_ipclient = kwargs['myclient']
    
    try: 
        return self_ipclient.score({'device': {'ip_address': self_ip}})
    except Exception as e:
        return(f"Failed to lookup IP Score: {e}")

def _iplookup(**kwargs):
    self_ip = kwargs['myip']
    self_ipclient = kwargs['myclient']
    
    try: 
        return self_ipclient.insights(self_ip)
    except Exception as e:
        return(f"Failed to lookup IP: {e}")

def _ipclient(**kwargs):
    self_maxuserid = kwargs['mymaxuserid']
    self_maxkey = kwargs['mymaxkey']

    try:
        return geoip2.webservice.Client(self_maxuserid, self_maxkey)
    except Exception as e:
        return(f"Failed to build geoip2 client: {e}")

def _minfraudclient(**kwargs):
    self_maxuserid = kwargs['mymaxuserid']
    self_maxkey = kwargs['mymaxkey']

    try:
        return Client(self_maxuserid, self_maxkey)
    except Exception as e:
        return(f"Failed to build minfraud client: {e}")

def ip_lookup(ip_address):
  logging.info(f"trying to looking up {primary_key}:{ip_address} item in {ddb_table}")
  try:
    response = db_resource.get_item(
        Key={
            primary_key: ip_address
        }
    )
  except Exception as e:
    logging.info(f"FAILED GetItem, DynamoDB {ddb_table} could not be found. {e}")
    return False
  else:
    if 'Item' not in response.keys():
      logging.info(f"FAILED GetItem, No match for {ip_address} found in {ddb_table}. Returned Item {response}")
      return False
    else:
      try:
        response = eval(ujson.dumps(response['Item']))
      except Exception as e:
        logging.info(f"FAILED to parse {response} to dict {e}")
        return False
      else:
        logging.info(f"SUCCESSFUL GetItem {response}")
        return response['data']

def ip_put(ip_address, json_response):
  logging.info(f"trying to insert {ip_address} item in {ddb_table}")
  try:
    response = db_resource.put_item(
      Item={
        primary_key: ip_address,
        'data': json_response,
      }
    )
    return True
  except Exception as e:
    logging.info(f"FAILED PutItem {response}")
    return False

# for local testing
#event = "{'body-json': {'ip': '24.23.23.23'}, 'params': {'path': {'proxy': 'prd'}, 'querystring': {}, 'header': {'Accept': '*/*', 'CloudFront-Forwarded-Proto': 'https', 'CloudFront-Is-Desktop-Viewer': 'true', 'CloudFront-Is-Mobile-Viewer': 'false', 'CloudFront-Is-SmartTV-Viewer': 'false', 'CloudFront-Is-Tablet-Viewer': 'false', 'CloudFront-Viewer-Country': 'US', 'content-type': 'application/json', 'Host': 'api.belial.io', 'User-Agent': 'curl/7.54.0', 'Via': '2.0 62c9e714a19291e1725b1320d4c9b369.cloudfront.net (CloudFront)', 'X-Amz-Cf-Id': 'w4aWN0X9s4Go9WZD052vrPQiuvbsLLWc3LXEv6VD1tJM4TlmtAzcTQ==', 'X-Amzn-Trace-Id': 'Root=1-5e5c5ad4-272f4c0d909d92ef19b96b7f', 'x-api-key': '5plVLaQVgL5NY0HTXkFOo6lBwHGZBck19BNpLMf1', 'X-Forwarded-For': '24.15.215.156, 70.132.37.146', 'X-Forwarded-Port': '443', 'X-Forwarded-Proto': 'https'}}, 'context': {'api-id': 'hcr6smzs03', 'api-key': '5plVLaQVgL5NY0HTXkFOo6lBwHGZBck19BNpLMf1', 'http-method': 'POST', 'stage': 'prd', 'source-ip': '24.15.215.156', 'user-agent': 'curl/7.54.0'}}"
#dict_event = eval(event)
#context = "<bootstrap.LambdaContext object at 0x7f7594677d50>"

# for local testing
#def _main(event, context):
def lambda_handler(event, context):
    self_exp = "^Failed to"
    self_default = "NA"
    iplookup_failed = False
    json_response = {}

    logging.info(f"ENV VARIABLES: MAXUSERID={maxuserid}, MAXKEY={maxkey}, DDB_TABLE={ddb_table}, DDB_KEY={primary_key}, AWS_REGION={aws_region}")
    logging.info(f"event: {event}")
    logging.info(f"context: {context}")
    logging.info(f"ip: {event['body-json']['ip']}")

    cur_time = int(time.time())
    table_lookup = ip_lookup(event['body-json']['ip'])
    logging.info(f"{table_lookup}")

    if (table_lookup is False):
      logging.info(f"no match found in table")
    else:
      logging.info(f"match found {table_lookup}")
      table_lookup_dict = ujson.loads(table_lookup)
      logging.info(f"type: {type(table_lookup_dict)}")
      logging.info(f"event: {table_lookup_dict}")
      if (cur_time > int(table_lookup_dict['expire_date'])):
        logging.info(f"item has expired, looking up again")
        table_lookup = False
      else:
        return(table_lookup_dict)

    self_ipclient = _ipclient(mymaxuserid=maxuserid, mymaxkey=maxkey)
    if (re.search(self_exp, str(self_ipclient))):
        iplookup_failed = True
        json_response['ipclient_error'] = self_ipclient
    iplook_response = _iplookup(myclient=self_ipclient, myip=event["body-json"]["ip"])

    self_minfraudclient = _minfraudclient(mymaxuserid=maxuserid, mymaxkey=maxkey)
    if (re.search(self_exp, str(self_minfraudclient))):
        iplookup_failed = True
        json_response['minfraud_client_error'] = self_minfraudclient
    minfraud_response = _ipscore(myclient=self_minfraudclient, myip=event["body-json"]["ip"])

    if (re.search(self_exp, str(iplook_response))): 
        iplookup_failed = True
        json_response['iplookup_error'] = iplook_response
    else:
        if iplook_response.continent:
            if iplook_response.continent.code is None:
                json_response['continent_code'] = self_default
            else:
                json_response['continent_code'] = iplook_response.continent.code

            if iplook_response.continent.geoname_id is None:
                json_response['continent_geoname_id'] = self_default
            else:
                json_response['continent_geoname_id'] = iplook_response.continent.geoname_id

            if iplook_response.continent.name is None:
                json_response['continent_name'] = self_default
            else:
                json_response['continent_name'] = iplook_response.continent.name

        if iplook_response.country:
            if iplook_response.country.confidence is None:
                json_response['country_confidence'] = self_default
            else:
                json_response['country_confidence'] = iplook_response.country.confidence

            if iplook_response.country.geoname_id is None:
                json_response['country_geoname_id'] = self_default
            else:
                json_response['country_geoname_id'] = iplook_response.country.geoname_id

            if iplook_response.country.is_in_european_union is None:
                json_response['country_is_in_european_union'] = self_default
            else:
                json_response['country_is_in_european_union'] = iplook_response.country.is_in_european_union
                
            if iplook_response.country.iso_code is None:
                json_response['country_iso_code'] = self_default
            else:
                json_response['country_iso_code'] = iplook_response.country.iso_code

            if iplook_response.country.name is None:
                json_response['continent_name'] = self_default
            else:
                json_response['continent_name'] = iplook_response.country.name

        if iplook_response.city:
            if iplook_response.city.confidence is None:
                json_response['city_confidence'] = self_default
            else:
                json_response['city_confidence'] = iplook_response.city.confidence

            if iplook_response.city.geoname_id is None:
                json_response['city_geoname_id'] = self_default
            else:
                json_response['city_geoname_id'] = iplook_response.city.geoname_id

            if iplook_response.city.name is None:
                json_response['city_name'] = self_default
            else:
                json_response['city_name'] = iplook_response.city.name

        if iplook_response.location:
            if iplook_response.location.average_income is None:
                json_response['location_average_income'] = self_default
            else:
                json_response['location_average_income'] = iplook_response.location.average_income
            
            if iplook_response.location.accuracy_radius is None:
                json_response['location_accuracy_radius'] = self_default
            else:
                json_response['location_accuracy_radius'] = iplook_response.location.accuracy_radius
            
            if iplook_response.location.latitude is None:
                json_response['location_latitude'] = self_default
            else:
                json_response['location_latitude'] = iplook_response.location.latitude

            if iplook_response.location.longitude is None:
                json_response['location_longitude'] = self_default
            else:
                json_response['location_longitude'] = iplook_response.location.longitude
            
            if iplook_response.location.metro_code is None:
                json_response['location_metro_code'] = self_default
            else:
                json_response['location_metro_code'] = iplook_response.location.metro_code
            
            if iplook_response.location.population_density is None:
                json_response['location_population_density'] = self_default
            else:
                json_response['location_population_density'] = iplook_response.location.population_density

            if iplook_response.location.time_zone is None:
                json_response['location_time_zone'] = self_default
            else:
                json_response['location_time_zone'] = iplook_response.location.time_zone

        if iplook_response.postal:
            if iplook_response.postal.code is None:
                json_response['postal_code'] = self_default
            else:
                json_response['postal_code'] = iplook_response.postal.code
            
            if iplook_response.postal.confidence is None:
                json_response['postal_confidence'] = self_default
            else:
                json_response['postal_confidence'] = iplook_response.postal.confidence

        if iplook_response.subdivisions:
            if iplook_response.subdivisions.most_specific.confidence is None:
                json_response['subdivisions_confidence'] = self_default
            else:
                json_response['subdivisions_confidence'] = iplook_response.subdivisions.most_specific.confidence

            if iplook_response.subdivisions.most_specific.geoname_id is None:
                json_response['subdivisions_geoname_id'] = self_default
            else:
                json_response['subdivisions_geoname_id'] = iplook_response.subdivisions.most_specific.geoname_id

            if iplook_response.subdivisions.most_specific.iso_code is None:
                json_response['subdivisions_iso_code'] = self_default
            else:
                json_response['subdivisions_iso_code'] = iplook_response.subdivisions.most_specific.iso_code

            if iplook_response.subdivisions.most_specific.name is None:
                json_response['subdivisions_name'] = self_default
            else:
                json_response['subdivisions_name'] = iplook_response.subdivisions.most_specific.name

        if iplook_response.traits:
            if iplook_response.traits.autonomous_system_number is None:
                json_response['autonomous_system_number'] = self_default
            else:
                json_response['autonomous_system_number'] = iplook_response.traits.autonomous_system_number

            if iplook_response.traits.autonomous_system_organization is None:
                json_response['autonomous_system_organization'] = self_default
            else:
                json_response['autonomous_system_organization'] = iplook_response.traits.autonomous_system_organization

            if iplook_response.traits.connection_type is None:
                json_response['connection_type'] = self_default
            else: 
                json_response['connection_type'] = iplook_response.traits.connection_type

            if iplook_response.traits.domain is None:
                json_response['domain'] = self_default
            else:
                json_response['domain'] = iplook_response.traits.domain

            if iplook_response.traits.ip_address is None:
                json_response['ip_address'] = ip_address = self_default
            else:
                json_response['ip_address'] = ip_address = iplook_response.traits.ip_address

            if iplook_response.traits.is_anonymous is None:
                json_response['is_anonymous'] = self_default
            else:
                json_response['is_anonymous'] = iplook_response.traits.is_anonymous

            if iplook_response.traits.is_anonymous_proxy is None:
                json_response['is_anonymous_proxy'] = self_default
            else:
                json_response['is_anonymous_proxy'] = iplook_response.traits.is_anonymous_proxy

            if iplook_response.traits.is_anonymous_vpn is None:
                json_response['is_anonymous_vpn'] = self_default
            else:
                json_response['is_anonymous_vpn'] = iplook_response.traits.is_anonymous_vpn

            if iplook_response.traits.is_hosting_provider is None:
                json_response['is_hosting_provider'] = self_default
            else:
                json_response['is_hosting_provider'] = iplook_response.traits.is_hosting_provider

            if iplook_response.traits.is_legitimate_proxy is None:
                json_response['is_legitimate_proxy'] = self_default
            else: 
                json_response['is_legitimate_proxy'] = iplook_response.traits.is_legitimate_proxy

            if iplook_response.traits.is_public_proxy is None:
                json_response['is_public_proxy'] = self_default
            else: 
                json_response['is_public_proxy'] = iplook_response.traits.is_public_proxy

            if iplook_response.traits.is_satellite_provider is None:
                json_response['is_satellite_provider'] = self_default
            else: 
                json_response['is_satellite_provider'] = iplook_response.traits.is_satellite_provider

            if iplook_response.traits.is_tor_exit_node is None:
                json_response['is_tor_exit_node'] = self_default
            else: 
                json_response['is_tor_exit_node'] = iplook_response.traits.is_tor_exit_node

            if iplook_response.traits.isp is None:
                json_response['isp'] = self_default
            else: 
                json_response['isp'] = iplook_response.traits.isp

            if iplook_response.traits.organization is None:
                json_response['organization'] = self_default
            else: 
                json_response['organization'] = iplook_response.traits.organization

            if iplook_response.traits.user_type is None:
                json_response['user_type'] = self_default
            else: 
                json_response['user_type'] = iplook_response.traits.user_type

    if (re.search(self_exp, str(minfraud_response))): 
        iplookup_failed = True
        json_response['minfraud_response_error'] = minfraud_response
    else:
        if minfraud_response: 
            if minfraud_response.risk_score is None:
                json_response['score_risk_score'] = self_default
            else:
                json_response['score_risk_score'] = minfraud_response.risk_score

            if minfraud_response.ip_address is None:
                json_response['score_ip_address'] = self_default
            else:
                json_response['score_ip_address'] = minfraud_response.ip_address

    if iplookup_failed:
        json_response['queries_remaining'] = self_default
    else:
        json_response['queries_remaining'] = iplook_response.maxmind.queries_remaining

    if table_lookup is False and iplookup_failed is False:
      json_response['insert_date'] = cur_time
      json_response['expire_date'] = (cur_time + 2592000)
      json_load = ujson.dumps(json_response)
      ip_put(event['body-json']['ip'], json_load)     
    return(json_response)

# for local testing
#_main(dict_event, context)
