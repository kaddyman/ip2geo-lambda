# ip2geo-lambda 

A lambda triggered by api-gateway that accepts post data `{"ip": "7.90.23.1"}` 
and returns an ip's location and fraud score via maxmind. It will also cache 
ip results for 30 days in a dynamoDB table. Results are returned in json.

### Build/Deploy

The Makefile can be used to build the zip file and deploy lambda code updates.

### bashrc function

Once you have your api up and running you can use something like the below 
in your bash_profile/bashrc to make calls to it.

```bash
function geoip(){
  curl -s -X POST -H 'x-api-key: 5plVLaQVgL5NY0HTXkFOo6lBwHGZBck19BNpLMf1' -H 'Content-Type: application/json' -d'{"ip": "'${1:-}'"}' 'https://api.belial.io/geoip/prd/' | jq
}
```

### Notes

The API key used is NOT an active key. Only for exmaple. 


If you don't want to run your own api you can always use this script I 
wrote to get ip lookup info from maxmind. 


https://gitlab.com/kaddyman/ip2geo
